#!/usr/bin/env python3
from os import mkdir
from pathlib import Path
from numpy.random import default_rng
from numpy import zeros, empty
import logging

# Configure the logging facility and get a default logger
logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger('')

# Define the path to the root of the repository
ROOT_DIR = Path(__file__).parent

# Define the path to the file where the results and figures will be stored
DATA_DIR = Path(ROOT_DIR, 'Data')
DATA_PATH = Path(DATA_DIR, 'records.lz4')
FIGS_DIR = Path(ROOT_DIR, 'Figures')

# Make sure the Data directory exists
if not DATA_DIR.is_dir():
    mkdir(DATA_DIR, mode=0o766)


# Make sure the Figures directory exists
if not FIGS_DIR.is_dir():
    mkdir(FIGS_DIR, mode=0o766)

# Define the (seeded) random number generator for numpy
NUMPY_SEED = 3  # If set to None, numpy will use an OS-dependent source of randomness
if NUMPY_SEED is not None:
    RNG = default_rng(NUMPY_SEED)
else:
    RNG = default_rng()

# Declare the experiment's parameters
# CS_IDX = None  # 0-based index of the frequency selected as the CS. If set to None, the CS is chosen randomly at runtime
CS_IDX = 6
NB_FREQUENCIES = 15
LAT_INHIB = 0.2
ACT_THRES = 0
ACT_SAT = 1
LR_RATE = 0.1

# Declare the size of the different layers
SIZES = {'mgm': 3, 'mgv': 8, 'cortex': 8, 'amygdala': 3}

# Define the WEIGHTS for the connections between the LAYERS
WEIGHTS = {
    'mgm': RNG.random(size=(NB_FREQUENCIES + 2, SIZES['mgm'])),
    'mgv': RNG.random(size=(NB_FREQUENCIES + 1, SIZES['mgv'])),
    'cortex': RNG.random(size=(SIZES['mgm'] + SIZES['mgv'], SIZES['cortex'])),
    'amygdala': RNG.random(size=(SIZES['mgm'] + SIZES['cortex'] + 1, SIZES['amygdala']))
}

# Declare a dictionary holding the output activities of the
# different layers
LAYERS = {k: zeros(v,) for k, v in SIZES.items()}

RECORDS = {k: {'pre': empty((NB_FREQUENCIES, v + 1)),
               'post': empty((NB_FREQUENCIES, v + 1))} for k, v in SIZES.items()}
