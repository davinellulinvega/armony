# Description
The source code in this repository is a Python based reproduction of the experiment by Armony et al. [^armony1995]. My attempt a reproducing both the model and results described in the original work has been published in the ReScience C journal [^caspar2021].

Before guiding you through the steps to install and run the program yourself, this first section gives an overview of the hypotheses, the model, and the experiment introduced in the original paper.

## Goals
The article by Armony et al. [^armony1995] introduces a computational model of the *'Two-pathways'* to the amygdala, as suggested by LeDoux [^LeDoux1992], and replicate a classical conditioning experiment to confirm the validity of the three basic assumptions on which the model is built. Those assumptions are:
1. Processing units: The average activity of a small population of biological neurons can be represented by a non-linear summing device,
2. Dual-pathway connectivity: To reach the amygdala from the thalamus a signal has to follow two parallel pathways, an indirect one which contains elaborate information, and a direct one which processes cruder representations,
3. Learning: Modification of the connection strengths between groups of processing units follows a competitive learning algorithm which involves a modified Hebbian learning rule as well as a winner-takes-all mechanism,

The model's adequacy is determined by its ability to reproduce physiological and behavioral data from classical conditioning studies.

The second goal of the original paper was to show that computational models provide a framework to not only confirm the consistency of the mechanisms suggested to explain the behavior and the neural observations of specific brain systems, but also make testable predictions which can be used to move neuroscience forward.

## Model
### Two pathways to the amygdala
The theory of the *'Two-pathways'* to the amygdala has been suggested by LeDoux [^LeDoux1992], and further explored by Romanski and LeDoux [^romanski1993a],[^romanski1993b]. It describes the idea that during conditioning, any stimulus entering the brain reaches the part of the thalamus that corresponds to its sensory modality.

In the context of the experiment implemented by Armony et al. [^armony1995], the Conditioned Stimulus (CS) was a tone of a specific frequency, while the Unconditioned Stimulus (US) was interpreted as a footshock. Therefore, in this specific case the portion of the thalamus targeted by incoming auditory signals is the medial geniculate body (MGB). The signal reaching this thalamic area is then forwarded to the amygdala via two pathways:
1. The first one connects the thalamus directly to the amygdala, and is thought to transmit unrefined representations of the incoming stimulus.
	For this experiment, since the CS is a tone the first pathway has been found to originate in the medial division of the MGB (MGm).
2. The second pathway, emerging from the ventral division of the MGB (MGv), sends the stimulus to its corresponding cortical system, in our case that would be the auditory cortex, which processes the stimulus to build a more elaborate representation. Finally, this detailed representation is transmitted to the amygdala.

### Implementation in artificial neural populations
Based on their assumptions, Armony et al. [^armony1995] built a computational model using non-linear summing devices, whose activation functions are thresholded ramps, grouped according to the different brain areas involved in the transmission of the auditory signal from the thalamus to the amygdala.

As such the model is made of four groups (not counting the input) of artificial neurons, linked together using feed forward excitatory connections:
+ Receiving auditory stimuli directly from the input are the **MGm** and **MGv** divisions of the thamalus.
+ The **MGm** connects directly to the **Amygdala**,
+ while the **MGv** sends its output to the **Auditory Cortex**, which in turn targets the **Amygdala**.

An additional binary unit representing the US connects directly to all neurons in both the MGm and Amygdala layers.

To mimic the presence of inter-neurons, processing units within a group are connected to each other via lateral inhibitory projections. This results in each processing unit developing a *'receptive field'*. A receptive field is defined as the set of adjacent frequencies for which the neuron's output is non-zero. In addition to the lateral inhibition described above, competitive learning is achieved using a modified Hebbian learning algorithm called the Stent-Hebb rule [^stent1973].

## Experiment
As mentioned above, the adequacy of the model was determined by its ability to replicate physiological and behavioral results from classical condition studies. Consequently, Armony et al. [^armony1995] simulated a conditioning paradigm to measure the neural activities across the whole network.

For reasons that will be explained later, the simulation is split into two main phases: a development and a conditioning phase.

### Development phase
Rather than constrain the distribution of the different synaptic weights during the initialization phase, and risk unforeseen consequences in the results, the model is first presented with all the tones sequentially. During this phase the nociceptive US unit was turned **Off** for all inputs.

The network is run until all processing units develop stable receptive fields.

### Conditioning phase
The conditioning phase is identical to the development phase, save for the fact that a single input frequency is arbitrarily selected as the CS to be associated with the US. Consequently, the model is again presented with all the tones sequentially. However, this time the nociceptive US unit is turned **On** when the CS is supplied as input to the model. The US is turned back **Off** for all other frequencies.

Again the network is run until the receptive fields of all processing units reach equilibrium.

### Testing phase
Testing happens after both the Development and Conditioning phase.
During this phase, two quantities are measured:
1. The behavioral output, which is defined as the sum total activity of the amygdala's neurons,
2. The receptive field for all processing units.

Measuring the neurons' receptive fields entails providing all input frequencies to the model sequentially and recording the activity of all neural units across the model.

Both measures are used to validate the hypotheses formulated by Armony et al. [^armony1995], as well as compare our results to the original work.

# Installation
This section details the steps required to setup an environment in which the simulated conditioning paradigm can be run.

## Dependencies
The simulation depends of the following software:
+ [Python](https://www.python.org/downloads/) >= 3.4
+ [HDF5](https://www.hdfgroup.org/downloads/hdf5/) >= 1.8.5
+ [Virtualenv](https://virtualenv.pypa.io/en/latest/index.html)
+ [Pip](https://pypi.org/)
+ [Git](https://git-scm.com/)

0. Make sure that Git, Python, HDF5, and virtualenv (optional) are installed on your system. On ubuntu, this can be done using the following command line:
```bash
$ sudo apt-get install python3 python3-pip hdf5-tools python3-virtualenv git
```

## Downloading the source code
1. The source code for the conditioning experiment is available on [Gitlab](https://gitlab.com/davinellulinvega/armony). To download it on your computer open a terminal and enter the following command:
```bash
> git clone https://gitlab.com/davinellulinvega/armony.git ReArmony
```
This will download the repository and place it in a folder called `ReArmony`.

2. Before continuing, move into the newly created directory:
```bash
> cd ReArmony
```

## Virtual environment (optional)
The next two steps are optional, but highly recommended.

3. To avoid mixing your system's packages with those required by this project, create a new virtual environment using the following command:
```bash
> virtualenv rearmony
```

4. Then activate said virtual environment:
```bash
> source rearmony/bin/activate
```

/!\ After running the experiment and extracting the results, to exit the virtual environment you can simply `deactivate` it. No parameters required:
```bash
> deactivate
```

## Requirements
The two programs making up this project relies on the following Python libraries:
+ [Numpy](https://numpy.org/)
+ [PyTables](https://www.pytables.org/index.html)
+ [Pandas](https://pandas.pydata.org/)
+ [Matplotlib](https://matplotlib.org/stable/index.html)

5. To install those libraries, once your virtual environment is activated, run the following command:
```bash
> pip install -r requirements.txt
```

# Usage
As mentioned above, this project is split into two scripts. The `main.py` program executes both phases of the conditioning experiment, and records the physiological and behavioral data.

The second, `display.py`, script extract the experimental results and builds all the figures shown in both the original and replicated articles.

To run the main experiment simply enter the following command:
```bash
> python main.py
```

Once a conditioning simulation has been completed it is possible to generate figures from the experimental results using the following command:
```bash
> python display.py
```

## Configuration
All parameters related to the simulated classical conditioning experiment (such as the learning rate, random seed, number of neurons per layer, and so on) can be found and modified in the `settings.py` file. Users are referred to the comments accompanying those parameters for an explanation on their significance and use in the main program.

# Issues
If you encounter any bugs or other problems while running any of the two scripts in this repository, please fill in a [report](https://gitlab.com/davinellulinvega/armony/-/issues/new).

# License
The entirety of the source code within this repository can be used following the conditions dictated by the [GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html).

---
# References
[^armony1995]: Armony, J. L., Servan-Schreiber, D., Cohen, J. D., & LeDoux, J. E. (1995). An anatomically constrained neural network model of fear conditioning. _Behavioral Neuroscience_, _109_(2), 246–257. [https://doi.org/10.1037/0735-7044.109.2.246](https://doi.org/10.1037/0735-7044.109.2.246)

[^stent1973]: Stent, G. S. (1973). A Physiological Mechanism for Hebb’s Postulate of Learning. _Proceedings of the National Academy of Sciences_, _70_(4), 997–1001. [https://doi.org/10.1073/pnas.70.4.997](https://doi.org/10.1073/pnas.70.4.997)

[^romanski1993a]: Romanski, L. M., & LeDoux, J. E. (1993). Information cascade from primary auditory cortex to the amygdala: Corticocortical and corticoamygdaloid projections of temporal cortex in the rat. _Cerebral Cortex (New York, N.Y.: 1991)_, _3_(6), 515–532. [https://doi.org/10.1093/cercor/3.6.515](https://doi.org/10.1093/cercor/3.6.515)

[^romanski1993b]: Romanski, L. M., & LeDoux, J. E. (1993). Organization of rodent auditory cortex: Anterograde transport of PHA-L from MGv to temporal neocortex. _Cerebral Cortex (New York, N.Y.: 1991)_, _3_(6), 499–514. [https://doi.org/10.1093/cercor/3.6.499](https://doi.org/10.1093/cercor/3.6.499)

[^romanski1992]: Romanski, L. M., & LeDoux, J. E. (1992). Equipotentiality of thalamo-amygdala and thalamo-cortico-amygdala circuits in auditory fear conditioning. _Journal of Neuroscience_, _12_(11), 4501–4509.

[^LeDoux1992]: LeDoux, J. E. (1992). Brain mechanisms of emotion and emotional learning. _Current Opinion in Neurobiology_, _2_(4), 191–197. [https://doi.org/10.1016/0960-9822(92)90530-N](https://doi.org/10.1016/0960-9822(92)90530-N)

[^caspar2021]: To be published
