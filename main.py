#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'Luc Caspar'

import numpy as np
import pandas as pd
from settings import LAYERS, WEIGHTS, LAT_INHIB, ACT_SAT, ACT_THRES, RECORDS, RNG, LR_RATE, SIZES, DATA_PATH, \
    NB_FREQUENCIES, LOG, CS_IDX


def propagation(layer_name, sending_units):
    """
    Propagate the output activities of the sending units to the given layer.
    This is a simple weighted sum, which in matrix form boils down to a dot
    product between the input and the weights.
    :param layer_name: String. A string representing the name of the
    receiving layer.
    :param sending_units: Numpy.array. An array containing the activation
    outputs of the sending units.
    :return: Nothing.
    """

    # Apply a simple dot product and store the result in the dictionary of
    # layers
    LAYERS[layer_name] = np.dot(sending_units, WEIGHTS[layer_name])


def lateral_inhibition(layer_name):
    """
    Implement a mechanism similar to a winner-takes-all, to restrict the
    number of activated units for a given input, and force the development of
    feature detectors.
    :param layer_name: String. The name of the layer on which to apply the
    lateral inhibition.
    :return: Nothing.
    """

    # Record the index and value of the maximum output activity
    idx_max, max_net = np.argmax(LAYERS[layer_name]), np.max(LAYERS[layer_name])
    # Inhibit all units in the layer
    LAYERS[layer_name] -= LAT_INHIB * max_net
    # Restore the value of the wining unit
    LAYERS[layer_name][idx_max] = max_net


def activation(layer_name):
    """
    Apply a non-linear function to the activities of the receiving units.
    :param layer_name: String. A string representing the name of the layer on
    which to apply the activation function.
    :return: Nothing.
    """

    # Compute the difference between the activity of each unit and the threshold
    LAYERS[layer_name] -= ACT_THRES
    # Rectify the value of the units that are beyond the saturation or below
    # zero
    LAYERS[layer_name] = np.where(LAYERS[layer_name] < 0, 0, LAYERS[layer_name])
    LAYERS[layer_name] = np.where(LAYERS[layer_name] > ACT_SAT, ACT_SAT,
                                  LAYERS[layer_name])


def learning(layer_name, sending_units):
    """
    Adjust the connection weights between the sending and receiving units,
    following a modified Hebbian learning rule.
    :param layer_name: String. A string representing the layer,
    whose connections should be adjusted.
    :param sending_units: Numpy.array. A numpy array containing the value of
    the activation of the sending units.
    :return: Nothing.
    """

    # Compute the average activation of the sending layer
    act_avg = np.mean(sending_units)

    # For each column (corresponding to a receiving unit)
    for i in range(LAYERS[layer_name].shape[0]):
        # Adjust the connection WEIGHTS
        WEIGHTS[layer_name][:, i] = np.where(sending_units > act_avg,
                                             WEIGHTS[layer_name][:, i] + LR_RATE * sending_units * LAYERS[layer_name][i],
                                             WEIGHTS[layer_name][:, i])

        # Normalize the connection weights
        WEIGHTS[layer_name][:, i] = WEIGHTS[layer_name][:, i] / np.sum(WEIGHTS[layer_name][:, i])


def step_layer(layer_name, sending_units, learn=True):
    """
    Propagate the activation of the sending units to the receiving units,
    and adjust the weight of the connections between the two layers.
    :param layer_name: String. A string representing the name of the
    receiving layer.
    :param sending_units: Numpy.array. A numpy array containing the activation
    values of the sending units.
    :param learn: Bool. A flag indicating whether learning should occur or not.
    :return: Nothing.
    """

    # Forward propagation
    propagation(layer_name, sending_units)

    # Lateral inhibition
    lateral_inhibition(layer_name)

    # Activation
    activation(layer_name)

    # Learning
    if learn:
        learning(layer_name, sending_units)


def step_phase(phase_name='', learn=True):
    """
    Execute a complete phase of the experiment, stepping through the
    different layers in order and adjusting the connections' weight.
    :param phase_name: String. A label identifying the current phase.
    :param learn: Bool. A flag indicating whether the weights of the
    connections between sending and receiving layers should be adjusted.
    :return: Nothing.
    """

    # Go through the sequence of frequencies to develop feature detectors
    for idx, tone in enumerate(inputs):
        # Step through the MGm layer
        step_layer('mgm', tone, learn=learn)

        # Step through the MGv layer
        step_layer('mgv', tone[:-1], learn=learn)

        # Step through the auditory cortex layer
        step_layer('cortex', np.hstack([LAYERS['mgm'], LAYERS['mgv']]), learn=learn)

        # Finally activate the amygdala layer
        step_layer('amygdala', np.hstack([LAYERS['mgm'], LAYERS['cortex'], tone[-1]]), learn=learn)

        if not learn:
            # Record the activation values of all units for the current
            # frequency
            for k, v in LAYERS.items():
                RECORDS[k][phase_name][idx] = np.hstack([idx + 1, v])


def extract_and_save():
    """
    Extract the recorded data, format it into wide data frames and save the
    data sets into HDF5 formatted files.
    :return: Nothing.
    """

    for k, v in SIZES.items():
        # Extract and compile the pre-conditioning data in a wide format
        pre = pd.DataFrame(data=RECORDS[k]['pre'], columns=['Frequency'] + ['Unit {}'.format(i+1) for i in range(v)])
        phase = pd.Series(data=['Pre-conditioning'] * NB_FREQUENCIES, name='Phase')
        pre = pd.concat([pre, phase], axis=1, sort=False)

        # Extract and compile the post-conditioning data in a wide format
        post = pd.DataFrame(data=RECORDS[k]['post'], columns=['Frequency'] + ['Unit {}'.format(i+1) for i in range(v)])
        phase = pd.Series(data=['Post-conditioning'] * NB_FREQUENCIES, name='Phase')
        post = pd.concat([post, phase], axis=1, sort=False)

        # Combine both data sets into one
        final = pd.concat([pre, post], axis=0, sort=False, ignore_index=True)

        # Write the final data frame to file
        final.to_hdf(DATA_PATH, key=k, complevel=9, mode='a',
                     complib='blosc:lz4', format='fixed')


if __name__ == '__main__':
    # Define the inputs corresponding to the contiguous frequencies along with
    # the US. The value of the US is stored in the last column.
    inputs = [np.zeros(NB_FREQUENCIES + 2,) for _ in range(NB_FREQUENCIES)]
    for i, arr in enumerate(inputs):
        arr[i:i+2] = 1

    # Development of feature detectors
    LOG.info('Executing the development phase ...')
    for _ in range(5000):
        step_phase()

    # Get a record of the feature detectors developed by the network
    step_phase(phase_name='pre', learn=False)
    LOG.info('Done')

    # Associate the US with a random CS
    if CS_IDX is None:
        rnd_idx = RNG.integers(0, len(inputs))
    else:
        rnd_idx = CS_IDX
    inputs[rnd_idx][-1] = 1
    LOG.info("Frequency chosen to be the CS: {}".format(rnd_idx + 1))

    LOG.info('Executing the conditioning phase ...')
    # Conditioning phase
    for _ in range(5000):
        step_phase()

    # Get a record of the new features detectors developed by the network
    step_phase(phase_name='post', learn=False)
    LOG.info('Done')

    LOG.info('Recording the results ...')
    # Format the recorded data into DataFrames and save it to file
    extract_and_save()
    LOG.info('Done')
