#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luc Caspar'

from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
from settings import FIGS_DIR, DATA_PATH, SIZES, LOG, CS_IDX


def plot_activity(layer_name, layer_size):
    """
    Graphs the output activities of each unit of a given layer into a single
    figure. Figures are saved to disk in PDF format.
    :param layer_name: String. The label uniquely identifying the layer.
    :param layer_size: Int. The number of units within the given layer.
    :return: Nothing.
    """

    # Compute the number of row and columns needed for the plots
    rows = max(1, layer_size // 4) * 2
    cols = min(layer_size, 4)

    # Create the figure along with its axes
    fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(12.8, 9.6),
                            squeeze=False, constrained_layout=True)
    # Set the figure's title
    fig.suptitle('Activation outputs '
                 'of the {} layer'.format(layer_name.capitalize()),
                 fontsize=18)

    # Extract the data from the pre- and post-conditioning phases
    pre = data[layer_name][data[layer_name].Phase == 'Pre-conditioning'].loc[:, 'Frequency': 'Unit {}'.format(layer_size)]
    post = data[layer_name][data[layer_name].Phase == 'Post-conditioning'].loc[:, 'Frequency': 'Unit {}'.format(layer_size)]
    # Reset the index of the post-conditioning dataFrame to align the data
    # with the pre-conditioning dataFrame
    post = post.set_index([pd.Series(range(15))])

    # Build a dataFrame with the difference between the post- and
    # pre-conditioning activities
    diff = pd.DataFrame(data=pre['Frequency'])

    # Plot all the things
    for i in range(layer_size):
        # Compute the difference in activity between the post- and
        # pre-conditioning phase
        diff['Post-Pre difference Unit {}'.format(i+1)] = post['Unit {}'.format(i + 1)] - pre['Unit {}'.format(i + 1)]
        # Get the current axes to graph our data to
        ax = axs[2 * (i // 4), i % 4]
        ax_diff = axs[2 * (i // 4) + 1, i % 4]

        # Plot the data corresponding to the pre-conditioning phase
        ax.plot('Frequency', 'Unit {}'.format(i + 1), data=pre,
                label='Pre')
        ax.plot('Frequency', 'Unit {}'.format(i + 1), data=post,
                label='Post', linestyle='dashed')

        ax.set_xlabel('Frequency', fontsize=18)
        ax.set_xlim(left=1, right=15, auto=None)
        ax.set_ylabel('Activity of unit {}'.format(i + 1), fontsize=18)
        ax.legend(fontsize=14)

        # Plot the difference between the post- and pre-conditioning activities
        ax_diff.plot('Frequency', 'Post-Pre difference Unit {}'.format(i+1),
                     data=diff, color='black')
        ax_diff.set_xlabel('Frequency', fontsize=18)
        ax_diff.set_xlim(left=1, right=15, auto=None)
        ax_diff.set_ylabel('Activity difference Unit {}'.format(i+1), fontsize=18)

    # Save the figure
    fig.savefig(Path(FIGS_DIR, 'activation_{}.png'.format(layer_name)),
                format='png', dpi=300)

    # Close the figure to save on memory
    plt.close(fig)


if __name__ == '__main__':
    # Make sure the file exist
    LOG.info('Looking for record file: {}'.format(DATA_PATH))
    if DATA_PATH.is_file():
        # Set the overall style used by matplotlib
        plt.style.use('seaborn-v0_8-whitegrid')

        # Retrieve all the data from the file
        data = {k: pd.read_hdf(DATA_PATH, key=k) for k in ['mgm',
                                                                'mgv',
                                                                'cortex',
                                                                'amygdala']}

        LOG.info('Generating the different figures ...')
        # Create and save the plots of the different units pre- and
        # post-conditioning
        for k, v in SIZES.items():
            plot_activity(k, v)

        # Extract the data corresponding to the behavioural output
        amyg = data['amygdala']
        pre = amyg[amyg['Phase'] == 'Pre-conditioning'].loc[:, 'Frequency': 'Unit 3']
        pre['Behavioral Output'] = pre.apply(lambda row: row['Unit 1':'Unit 3'].sum(), axis=1)
        post = amyg[amyg['Phase'] == 'Post-conditioning'].loc[:, 'Frequency': 'Unit 3']
        post['Behavioral Output'] = post.apply(lambda row: row['Unit 1':'Unit 3'].sum(), axis=1)

        diff = pd.DataFrame(data=pre['Frequency'])
        # Re-indexing the post dataFrame is necessary for the index of pre
        # and post to align and to be able to compute the difference
        post = post.set_index([pd.Series(range(15))])
        diff['Post-Pre difference'] = post['Behavioral Output'] - pre['Behavioral Output']

        # Plot the behavioural output
        fig, axs = plt.subplots(nrows=2, ncols=1, figsize=(12.8, 9.6),
                                constrained_layout=True)
        fig.suptitle('Overall response of the Amygdala (behavioral output)')
        axs[0].plot('Frequency', 'Behavioral Output', data=pre, label='Pre')
        axs[0].plot('Frequency', 'Behavioral Output', data=post, label='Post',
                    linestyle='dashed')
        axs[0].legend(fontsize=14)
        axs[0].set_xlabel('Frequency', fontsize=18)
        axs[0].set_xlim(left=1, right=15, auto=None)
        axs[0].set_ylabel('Behavioural Response', fontsize=18)

        # Plot the difference between the behavioral output pre- and
        # post-conditioning
        axs[1].bar('Pre', pre['Behavioral Output'][CS_IDX], width=0.4)
        axs[1].bar('Post', post['Behavioral Output'][CS_IDX], width=0.4)
        axs[1].set_ylabel('Behavioural Response', fontsize=18)

        # Save the figure
        fig.savefig(Path(FIGS_DIR, 'behavioral_response.png'),
                    format='png', dpi=300)

        # Close the figure to save on memory
        plt.close(fig)
        LOG.info('Done\nThe figures are available in {}'.format(FIGS_DIR))
    else:
        # Exit from the program, since there is nothing to do here
        LOG.error('Error: File not found.\nExiting the program ...')
